const express = require("express");

const app = express();



const Prueba = async(req, res) => {
    try {
        res.send({respuesta:'responde'});
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};



app.get("/api/catalogoDinamico/prueba", Prueba);



module.exports = app;