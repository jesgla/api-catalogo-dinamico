// Configuraciones
//require('./config/config');

// Importaciones
const express = require('express');
const cors = require('cors');
const app = express();

const bodyParser = require('body-parser');

// parse application/json
app.use(bodyParser.json());

//Cors
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
/**
 * Configuracion de cors para la app
 */
const corsOptions = {
    origin: (origin, callback) => {
        callback(null, true);
    }
  }

  app.options('*', cors(corsOptions));
  
  app.get('/', cors(corsOptions), (req, res, next) => {
    res.json({ message: 'This route is CORS-enabled for an allowed origin.' });
  })

// Rutas
app.use(require('./routes'));

app.listen(3000, () => {

    console.log('URL NODE:     http://localhost:' + 3000);
  
});